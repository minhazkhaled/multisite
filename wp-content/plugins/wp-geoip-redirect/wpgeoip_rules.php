<?php
/**
 * Function to manage rules into database
 */
function wpgeoip_admin()
{

    require_once 'geoip.inc';

	global $wpdb; 
	
	if(isset($_GET['delID']))
    {
        $ruleID = intval($_GET['delID']);
        $wpdb->query("DELETE FROM `".$wpdb->prefix."grules` WHERE `ruleID` = $ruleID");
        print '<meta http-equiv="refresh" content="0;url=index.php?page=wpgeoip-admin" />';
        exit;
    }

    $all_plugins = get_plugins();
    $_plugins = array();

    foreach($all_plugins as $plugin_name => $plugin_array) $_plugins[] = $plugin_name;

    $fl_array = preg_grep("/cache/i", $_plugins);

	?>
	<div id="wrap">
        <br />
        <img src="<?= plugin_dir_url(__FILE__) ?>/assets/images/icon32x32.png" style="float:left;"/> 
		<h2 style="float:left;margin-top: 10px;margin-left:10px;">WP GeoIP Redirect Rules</h2>
	    <div style="clear:both;"></div>
        <hr />
			
	<h3>Add New Redirect Rule</h3>
	
    <div class="updated below-h2">
    <?php if(count($fl_array)) echo '<h3 style="color:#cc0000;">If you have any CACHING plugins ACTIVE this plugin will not work properly simply because it will also cache the 1st visitor location and assume everyone else is from the same country. Ignore this message if it\'s not the case.!</h3>'; ?>
	<p><strong>NOTES!</strong><br/> - If you choose a category <strong>all traffic</strong> for that specific category will be redirected.<br/>
		- If you want to redirect <strong>a single POST</strong> LEAVE category as -None-<br/>
        - If you want to <strong>redirect</strong> no matter what category/page/post choose <strong>"SITEWIDE RULE"</strong>
	</p>
    </div>
    
    <?php
    if(isset($_POST['sbRule']))
    {
        if($_POST['target'] != "http://www." && $_POST['target'] != "" && 
            $_POST['country'] != "")
        {
            $country = esc_sql($_POST['country']);
            $target = esc_sql(trim($_POST['target']));
			$catID = intval($_POST['catID']);
			
            if($_POST['postID'] == 'home') {
            $postID = 0;
            $rs = $wpdb->query("INSERT INTO `".$wpdb->prefix."grules` (`countryID`,`targetURL`,
            					`catID`,`postID`,`home_rule`) VALUES 
                                ('$country', '$target','$catID','$postID', '1')");
			}else{
			$postID = intval($_POST['postID']);
            $rs = $wpdb->query("INSERT INTO `".$wpdb->prefix."grules` (`countryID`,`targetURL`,
            					`catID`,`postID`) VALUES 
                                ('$country', '$target','$catID','$postID')");
			}
                                
            print '<div class="updated below-h2">Rule successfully created!</div>';                    
            
        }else{
            print '<div class="updated below-h2">Country & Target URL must be specified</div>';
        }
    }
    ?>
    
    <form method="post">
        <table>
            <tr>
            <td>Country:</td>
            <td>
            <select name="country" class="chosen-select">
                <?php 
                $geoIP = new GeoIP;
                $countryList = array_combine($geoIP->GEOIP_COUNTRY_CODES, $geoIP->GEOIP_COUNTRY_NAMES);
                asort($countryList);
                $countryList = array_filter($countryList);
                foreach($countryList as $countryCode => $countryName) {
                    printf('<option value="%s">%s</option>', $countryCode, $countryName);
                }

                ?>    
            </select>
            </td>
            </tr>
            <tr>
            <td>For This Category:</td>
            <td><select name="catID" class="chosen-select">
                <option value="0">-None-</option>
                <?php
                $categories = get_categories();
				foreach($categories as $cat) {
                        print '<option value="'.$cat->cat_ID.'">'.$cat->name.'</option>';
                        print "\n";
                }
                ?>
                </select></td>
            </tr>
            <tr>
            <td>OR For This POST/PAGE</td>
            <td><select name="postID" class="chosen-select">
                <option value="0">-None-</option>
                <option value="999999">SITEWIDE RULE - ALL PAGES</option>
                <option value="home">!HOMEPAGE!</option>
                <?php
                $all_posts = get_posts('numberposts=-1&offset=0');
				foreach($all_posts as $post) {
                        print '<option value="'.$post->ID.'">'.$post->post_title.'</option>';
                        print "\n";
                }
				$all_pages = get_pages('numberposts=-1&offset=0');
				foreach($all_pages as $post) {
                        print '<option value="'.$post->ID.'">'.$post->post_title.'</option>';
                        print "\n";
                }
                $all_products = get_posts('numberposts=-1&offset=0&post_type=product');
                foreach($all_products as $post) {
                        print '<option value="'.$post->ID.'">Product: '.$post->post_title.'</option>';
                        print "\n";
                }
                $all_products = get_posts('numberposts=-1&offset=0&post_type=download');
                foreach($all_products as $post) {
                        print '<option value="'.$post->ID.'">Download: '.$post->post_title.'</option>';
                        print "\n";
                }

                ?>
                </select></td>
            </tr>
            <tr>
                <td>Target URL:</td>
                <td><input type="text" name="target" value="http://www." style="width:250px;" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="sbRule" value="Add Rule" class="button"/></td>
            </tr>
        </table>
        
        <hr noshade="" width="100%" />
        
        <h3>Current Rules</h3>
        
        <?php
        $rs = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."grules`");
        if(count($rs))
        {
            ?>
            <table width="100%" class="table widefat posts">
            <thead>
                <tr>
                    <th style="font-weight: bold;color: #01708C;">Country</th>
                    <th style="font-weight: bold;color: #01708C;">Target URL</th>
                    <th style="font-weight: bold;color: #01708C;">For Category/Post/Page</th>
                    <th style="font-weight: bold;color: #01708C;">Remove</th>
                </tr>
            </thead>
            <tbody>    
            <?php
            foreach($rs as $row)
            {
            	$postID = $row->postID;
				$catID = $row->catID;
				
				if($catID != 0)
				{
					$target = get_category($catID);
					$target = '<strong>Category</strong> : ' . $target->cat_name;
				}elseif($postID != 0){
                    if($postID != 999999) {
					   $target = get_post($postID);
					   $target = '<strong>'.ucfirst($target->post_type) . '</strong> : ' . $target->post_title;
                    }else{
                        $target = '<strong>SITEWIDE REDIRECT</strong>';
                    }
				}else{
					$target = "<strong>!HOMEPAGE!</strong>";
				}
				
                print '<tr>
                        <td>'.$countryList[$row->countryID].'</td>
                        <td>'.$row->targetURL.'</td>
                        <td>'.($target).'</td>
                        <td><a href="?page=wpgeoip-admin&delID='.$row->ruleID.'" onclick="return confirm(\'Are you SURE you want to REMOVE this redirect rule?\');">[x]</a>
                        </tr>';
            }
            ?>
            </tbody>
            </table>
            <?php   
        }else{
            print 'No rules yet!';
        }
        ?>
        
    </form>
		
	</div>	
	<?php
}
