<?php
/**
 * Redirect Function
 */
/*
 * Get User Public IP Address
 */

function WPGeoIP_getIP() {

  if (!empty($_SERVER['HTTP_CLIENT_IP']))
  //check ip from share internet
  {
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
  //to check ip is pass from proxy
  {
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else
  {
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
} 

function wpgeoip_redirect()
{

	if( get_option('wpgeoip_no_redirect', 0) == 1 AND isset($_GET['noredirect'])) return false;
	
	require_once 'geoip.inc';

	//open geoip binary database
	$gi = geoip_open(plugin_dir_path(__FILE__). 'GeoIP.dat', GEOIP_STANDARD);

	global $wpdb;
	global $wp_query;
	global $post;
	
	$prefix = $wpdb->prefix;
	
	$postID = $post->ID;
	
	$catID = intval($wp_query->query_vars['cat']);
	$isHome = is_home();
	
	$the_page_name = '';
	
	//get user country
	$countryCode = geoip_country_code_by_addr($gi, WPGeoIP_getIP());

	//sitewide rule
	$rs_redirect = $wpdb->get_row("SELECT `targetURL` FROM `".$prefix."grules` WHERE `countryID` = '$countryCode' 
							AND `postID` = 999999");
	if(isset($rs_redirect) AND (count($rs_redirect)))
	{
		$the_page_name = get_the_title($postID);
		$wpdb->query("INSERT INTO wpgeoip_log VALUES (null, 'SITEWIDE Redirect', 'Redirecting Country <strong>".$countryCode."</strong> to ".$rs_redirect->targetURL."')");
	    print '<meta http-equiv="refresh" content="0;url='.$rs_redirect->targetURL.'"/>';
	    exit;
	}
	
	//redirect if any rule for this country
	if($postID != 0) {
		$rs_redirect = $wpdb->get_row("SELECT `targetURL` FROM `".$prefix."grules` WHERE `countryID` = '$countryCode' 
							AND `postID` = $postID");
		$the_page_name = get_the_title($postID);							
	}
        if($catID != 0)
	{
		$rs_redirect = $wpdb->get_row("SELECT `targetURL` FROM `".$prefix."grules` WHERE `countryID` = '$countryCode' 
							AND `catID` = $catID");
		$the_page_name = 'Category : ' . get_the_category_by_ID($catID);
	}
        if($isHome){
		$rs_redirect = $wpdb->get_row("SELECT `targetURL` FROM `".$prefix."grules` WHERE `countryID` = '$countryCode' 
							AND `home_rule` = 1");
		$the_page_name = 'Homepage';
	}
        if(!$rs_redirect) {
		//NOTHING TO DO
		#$wpdb->query("INSERT INTO wpgeoip_log VALUES (null, 'Redirect', 'Nothing to do. No rules for Country <strong>".$countryCode."</strong>')");
	    }

		if(isset($rs_redirect) AND (count($rs_redirect)))
		{	
			$wpdb->query("INSERT INTO wpgeoip_log VALUES (null, 'Redirect <em>".$the_page_name."</em>', 'Redirecting Country <strong>".$countryCode."</strong> to ".$rs_redirect->targetURL."')");
	    	print '<meta http-equiv="refresh" content="0;url='.$rs_redirect->targetURL.'"/>';
	    	exit;
	}else{
	    //CHECK COUNTRIES WITHOUT REDIRECT RULES
	    $mass_redirect_enabled = get_option('wpgeoip_mass_redirect');
	    if($mass_redirect_enabled != "0")
		{
			$mass_url = get_option('wpgeoip_mass_url');
$wpdb->query("INSERT INTO wpgeoip_log VALUES (null, 'Mass Redirect', 'Redirecting Country <strong>".$countryCode."</strong> to ".$rs_redirect->targetURL."')");
			print '<meta http-equiv="refresh" content="0;url='.$mass_url.'"/>';
			exit;
			
		}else{
			//NOTHING TO DO AGAINM
		}
	}
	
}