<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'multisite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's_;U&V_+O%@}?20zaG6=Q5uw#u-8)vx78}a_@?{)%O-c~YeW|VG/it=2/|ps0f-?');
define('SECURE_AUTH_KEY',  ',1WqW9R:X+GX-74do~D+hOBVA0wF?^N9W(PiFt|UjL4fEH6m/<o)R|<>23$WV^T9');
define('LOGGED_IN_KEY',    'O%}Z8@Vk*TPAWL1BF0@1!ExD_<z}1Np8[VMf>u-<nZ)tm>^]5rG9VOg98>-<uU_v');
define('NONCE_KEY',        'R,t~[/x[xJoS+&4T8UqwW|8yY?_oH><3=P/DvVb/!3eucy-S[GK^;QnHQ-,Ic!X/');
define('AUTH_SALT',        '`Kr*1ueu^^)4ZMCNrb~Cw=<*(gcsZ4I:Nt*H!kZH~Sj;%*Zyv|v}?0;KF|/7F/AZ');
define('SECURE_AUTH_SALT', '=NYX_/-OM(=AOU#uEpngk(X;9!FP:I_h%B[%vbmphqow]%%xXW5chk>u+A,f|V|b');
define('LOGGED_IN_SALT',   'QTS)J^7VM3Izb|xS+H@}0[M:lz}G,UjFwHs{zeUGX]r9OU4{F~IeEXp!`.9fR+or');
define('NONCE_SALT',       '5X6;46F25hY]:>2i_<Qb7axv:q,3?y+X|A+.8UH;>*e^!>W~:XrPm9HW<!4cxbP~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true ); 
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/multisite/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
